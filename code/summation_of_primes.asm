section .data
    output: dq 0

section .text

global _start


_start: 

    call get_2m_prime                
    call set_variables
    call print_number
    call new_line
    call exit


get_2m_prime:

    mov r15, 2                            ; 2 primes (2 & 3)
    mov r12, 5                            ; the sum (2 + 3)
    mov rcx, 5                            ; number = start at 5

    loop:
    call is_prime
    cmp rax, 0                            ; if it isnt
    je continue                           ; continue
    add r12, rcx                          ; else, add the number

    continue:
    add rcx, 2                            ; number +2 (next odd)
    cmp rcx, 2000000                      ; if we havent 2.000.000 numbers
    jl loop                               ; continue
    ret


; is_prime(rcx:u64) -> rax(bool)
is_prime:

    mov r14, rcx                          ; save the number in r14
    mov [output], rcx                     ; mov the number into [mem]
    fild qword [output]                   ; Integer load from memory
    fsqrt                                 ; square root
    fisttp qword [output]                 ; Store Integer With Truncation (get the integer)


    mov rax, [output]                     ; mov the value into rax
    add rax, 1                            ; add 1 into rax
    mov rcx, 2                            ; 2..limit
    mov r13, rax                          ; save the limit in r13

    prime_loop:
    mov rax, r14                          ; mov the number into rax
    mov rbx, rcx                          ; mov the iter into rbx
    xor rdx, rdx                          ; clear remainder
    div rbx                               ; number / iter
    cmp rdx, 0                            ; if the remainder == 0
    je it_isnt_prime                      ; it isnt a prime
    inc rcx                               ; next number
    cmp rcx, r13                          ; if the iter != limit
    jne prime_loop                        ; continue the loop

    mov rax, 1                            ; else it is a prime
    jmp done                              ; jump done

    it_isnt_prime:
    mov rax, 0                            ; return false

    done:
    mov rcx, r14                          ; restore the number into rcx
    ret


print_number:

    call division

    cmp rax, 0                            ; if rax(result of div) == 0
    je check_if_is_need                   ; jump to check_if_is_need

    cmp rax, 0                            ; if rax(result of div) != 0
    jne modify_flag                       ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                            ; if im in the last iter
    je modify_flag                        ; modify_flag
    cmp r11, 0                            ; if the flag is false,
    je jump_the_write                     ; jump the write

    modify_flag:
    mov r11, 1                            ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                               ; decrement counter
    jnz print_number                      ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                           ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                            ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                          ; store number content into rax
    mov rbx, r13                          ; store the max divisor into rbx
    xor rdx, rdx                          ; clear remainder
    div rbx                               ; make the division
    mov r14, rax                          ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                          ; set the result of divison into rax
    mov rbx, r13                          ; set the current max divisor into rbx
    mul rbx                               ; great_number = result of division * current divisor
    sub r12, rax                          ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                          ; store divisor content into rax
    mov rbx, 10                           ; set 10 into rbx
    xor rdx, rdx                          ; clear remainder
    div rbx                               ; div divisor/10
    mov r13, rax                          ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                          ; add +48 to get numbers in ASCII
    mov [output], rax                     ; done, store result in "sum"
    mov rcx, 1                            ; len of a ascci char
    call write                            ; call write
    ret


new_line:

    mov rcx, 10                           ; mov \n to rcx
    mov [output], rcx                     ; mov \n to [output]
    mov rcx, 1                            ; \n len  
    call write                            ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                            ; write(
    mov rdi, 1                            ;   STDOUT_FILENO,
    mov rsi, output                       ;   text,
    mov rdx, rcx                          ;   sizeof(text)
    syscall                               ; );
    ret


exit:

    mov rax, 60                           ; exit(
    mov rdi, 0                            ;   EXIT_SUCCESS
    syscall                               ; );
