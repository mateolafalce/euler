section .data
    output: dq 0 

section .text

global _start


_start: 

    mov r15, 1                       ; a
    mov r14, 0                       ; b
    mov r13, 0                       ; c
    mov r12, 0                       ; a * a
    mov r11, 0                       ; b * b
    mov r10, 0                       ; c * c
    call get_number
    call set_variables
    call print_number
    call new_line
    call exit


get_number:

    a_loop:
    mov r14, r15                     ; b = a..(1000-a)

    b_loop:
    mov r13, 1000                    ; c = 1000
    sub r13, r15                     ; c = 1000 - a
    sub r13, r14                     ; c = 1000 - a - b

    mov rax, r15                     ; mov a into rax
    mul r15                          ; a * a
    mov r12, rax                     ; a * a -> r12

    mov rax, r14                     ; mov b into rax
    mul r14                          ; b * b
    mov r11, rax                     ; b * b -> r11

    mov rax, r13                     ; mov c into rax
    mul r13                          ; c * c
    mov r10, rax                     ; c * c -> r10

    add r12, r11                     ; a * a + b * b

    cmp r12, r10                     ; if a * a + b * b == c * c
    je done                          ; then its done

    inc r14                          ; b++
    mov rax, 1000                    ; rax -> 1000
    sub rax, r15                     ; 1000 - a
    cmp r14, rax                     ; if b isnt 1000 - a
    jne b_loop                       ; continue

    inc r15                          ; a++
    cmp r15, 1000                    ; if a isnt 1000
    jne a_loop                       ; continue

    done:
    mov rax, r15                     ; mov a into rax
    mul r14                          ; a * b
    mul r13                          ; a * b * c
    mov r12, rax                     ; result into r12
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
