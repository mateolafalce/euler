section .data
    output: dq 0 

section .text

global _start


_start: 

    call fivebyfive
    call set_variables
    call print_number
    call new_line
    call thobytho
    call set_variables
    call print_number
    call new_line
    call exit


fivebyfive:

    mov rax, 5                       ; set n
    mul rax                          ; n*n
    mov r15, rax                     ; mov into r15 the result
    call sum_diagonals               ; call it
    mov r12, rax                     ; mov to print
    ret


thobytho:

    mov rax, 1001                    ; set n
    mul rax                          ; n*n
    mov r15, rax                     ; mov into r15 the result
    call sum_diagonals               ; call it
    mov r12, rax                     ; mov to print
    ret


; sum_diagonals(r15 -> n * n) -> rax (sum)
sum_diagonals:

    xor rax, rax                     ; clear rax
    mov rcx, 1                       ; the first number
    mov rbx, 2                       ; the initial jumps
    mov r14, 0                       ; 0..4 diagonals
    add rax, rcx                     ; add 1 into rax

    loop:
    add rcx, rbx                     ; index += rbx
    add rax, rcx                     ; result += diagonal
    inc r14                          ; diagonals++
    cmp r14, 4                       ; if i take 4 diagonals
    je new_spiral                    ; the clear it
    jmp continue                     ; else continue

    new_spiral:
    xor r14, r14                     ; diagonals = 0
    add rbx, 2                       ; jumps += 2

    continue:
    cmp rcx, r15                     ; if iter != max
    jne loop                         ; continue
    ret


set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
