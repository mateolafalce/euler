section .data
    output: dq 0 

section .text

global _start


_start: 

    mov r12, 0                       ; store the sum of multiples in r12
    mov rcx, 0                       ; counter 0 -> 1000
    call get_number
    call set_variables
    call print_number
    call new_line
    call exit


get_number:

    xor rdx, rdx                     ; clear remainder
    mov rax, rcx                     ; mov current number into rax
    mov rbx, 3                       ; mov 3 into rbx
    div rbx                          ; rax / rbx
    cmp rdx, 0                       ; if isnt 0,
    jne check_div5                   ; go to check_div5
    add r12, rcx                     ; else, add the number to r12
    jmp next_iteration               ; then go to, next_iteration

    check_div5:    
    xor rdx, rdx                     ; clear remainder
    mov rax, rcx                     ; mov current number into rax
    mov rbx, 5                       ; mov 5 into rbx
    div rbx                          ; rax / rbx
    cmp rdx, 0                       ; if isnt 0,
    jne next_iteration               ; go to next_iteration
    add r12, rcx                     ; else, add the number to r12

    next_iteration:
    inc rcx                          ; rcx++
    cmp rcx, 1000                    ; if rcx isnt 0,
    jne get_number                   ; continue this loop
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000      ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
