section .data
    output: dq 0

section .text

global _start


_start: 

    call curious
    call set_variables
    call print_number
    call new_line
    call exit


curious:
    
    xor r12, r12
    mov rcx, 10

    curious_loop:
    call is_curious
    cmp rax, 1
    je add_one
    jmp else
    add_one:
    inc r12
    else:
    inc rcx
    cmp rcx, 2540160
    jne curious_loop 
    ret


; is_curious(rcx: number) -> rax
is_curious:

    xor r15, r15

    mov rax, rcx
    mov rbx, 1000000
    xor rdx, rdx
    div rbx

    call factorial
    add r15, rax

    mov rax, rcx
    mov rbx, 100000
    xor rdx, rdx
    div rbx

    call factorial
    add r15, rax

    mov rax, rcx
    mov rbx, 10000
    xor rdx, rdx
    div rbx

    call factorial
    add r15, rax

    mov rax, rcx
    mov rbx, 1000
    xor rdx, rdx
    div rbx

    call factorial
    add r15, rax

    mov rax, rcx
    mov rbx, 100
    xor rdx, rdx
    div rbx

    call factorial
    add r15, rax

    mov rax, rcx
    mov rbx, 10
    xor rdx, rdx
    div rbx

    call factorial
    add r15, rax

    mov rax, rdx
    call factorial
    add r15, rax

    xor r15, rcx
    cmp r15, 0
    je true

    mov rax, 0
    jmp cu_end

    true:
    mov rax, 1
    cu_end:

    ret


; factorial(rax) -> rax
factorial:

    cmp rax, 0
    je one
    cmp rax, 1
    je one

    mov rbx, rax
    factorial_loop:
    dec rbx
    mul rbx
    cmp rbx, 1
    jne factorial_loop
    jmp factorial_end

    one:
    mov rax, 1

    factorial_end:
    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
