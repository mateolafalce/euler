section .data
    output: dq 0

section .text

global _start


_start: 

    call get_ways
    mov r12, rdi
    call set_variables
    call print_number
    call new_line
    call exit


get_ways:

    xor r15, r15
    xor rdi, rdi
    mov r14, 0 ; 100
    mov r13, 0 ; 50
    mov r12, 0 ; 20
    mov r11, 0 ; 10
    mov r10, 0 ; 5
    mov r9, 0 ; 2
    mov r8, 0 ; 1

    loop_ways:

    mov rax, 100
    mov rbx, r14
    mul rbx
    add r15, rax

    mov rax, 50
    mov rbx, r13
    mul rbx
    add r15, rax

    mov rax, 20
    mov rbx, r12
    mul rbx
    add r15, rax

    mov rax, 10
    mov rbx, r11
    mul rbx
    add r15, rax

    mov rax, 5
    mov rbx, r10
    mul rbx
    add r15, rax

    mov rax, 2
    mov rbx, r9
    mul rbx
    add r15, rax

    mov rax, 1
    mov rbx, r8
    mul rbx
    add r15, rax

    cmp r15, 200
    je add_one
    jmp else

    add_one:
    inc rdi

    else:
    xor r15, r15

    inc r8
    cmp r8, 200
    jne loop_ways
    mov r8, 0

    inc r9
    cmp r9, 100
    jne loop_ways
    mov r9, 0

    inc r10
    cmp r10, 40
    jne loop_ways
    mov r10, 0

    inc r11
    cmp r11, 20
    jne loop_ways
    mov r11, 0

    inc r12
    cmp r12, 10
    jne loop_ways
    mov r12, 0

    inc r13
    cmp r13, 4
    jne loop_ways
    mov r13, 0

    inc r14
    cmp r14, 2
    jne loop_ways
    mov r14, 0

    end:
    add rdi, 8

    ret


get_divisor:

    xor r14, r14                     ; clear the output
    mov r15, rax                     ; mov rax into r15
    xor rcx, rcx                     ; clear the counter
    loop:
    inc rcx                          ; add the counter

    mov rax, r15                     ; mov r15 into rax
    mov rbx, rcx                     ; mov the counter into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx

    cmp rdx, 0                       ; if rdx inst 0
    jne continue                     ; continue
    add r14, rcx                     ; else add the counetr into r14

    continue:
    cmp rcx, r15                     ; if the counter isnt equal to the number
    jne loop                         ; continue
    sub r14, r15                     ; else sub the last number
    mov rax, r14                     ; mov the result into rax

    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
