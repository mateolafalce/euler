section .data
    output: dq 0

section .text

global _start


_start: 

    call get_amicable
    call set_variables
    call print_number
    call new_line
    call exit


get_amicable:

    xor r12, r12                     ; clear r12
    mov r13, 1                       ; init the counter
    loop_amicable:
    inc r13                          ; counter++

    mov rax, r13                     ; mov the counetr into rax
    call get_divisor                 ; get the divisor of counter
    mov r11, rax                     ; save the result into r11
    call get_divisor                 ; get the divisor of the last result

    cmp r13, r11                     ; if r13 isnt equal to r11
    jne verify_equal                 ; jump to verify_equal
    jmp continue_loop                ; else continue

    verify_equal: 
    cmp rax, r13                     ; if rax & r13 are equal
    je add_it                        ; jump to add
    jmp continue_loop                ; else continue

    add_it:
    add r12, r13                     ; add r13 to the result
    add r12, r11                     ; add r11 to the result

    continue_loop:
    cmp r13, 10000                   ; if r13 isnt equal to 10.000
    jne loop_amicable                ; continue

    ret


get_divisor:

    xor r14, r14                     ; clear the output
    mov r15, rax                     ; mov rax into r15
    xor rcx, rcx                     ; clear the counter
    loop:
    inc rcx                          ; add the counter

    mov rax, r15                     ; mov r15 into rax
    mov rbx, rcx                     ; mov the counter into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx

    cmp rdx, 0                       ; if rdx inst 0
    jne continue                     ; continue
    add r14, rcx                     ; else add the counetr into r14

    continue:
    cmp rcx, r15                     ; if the counter isnt equal to the number
    jne loop                         ; continue
    sub r14, r15                     ; else sub the last number
    mov rax, r14                     ; mov the result into rax

    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
