section .data
    output: dq 0 

section .text

global _start


_start: 

    call product_coe
    call set_variables
    call print_number
    call new_line
    call exit


product_coe:

    mov r14, -999                    ; a
    mov r12, 0                       ; prod
    mov r11, 0                       ; max_primes

    set_b:
    mov r15, -1000                   ; b

    set_n:
    mov r13, -1                      ; n

    new_loop: 
    inc r13                          ; n++
    mov rax, r13                     ; mov n into rax

    mul rax                          ; n * n
    mov rbx, rax                     ; n*n
    mov rax, r13                     ; mov n into rax
    mul r14                          ; a * n
    add rbx, rax                     ; n*n + a*n
    add rbx, r15                     ; n*n + a*n + b
    mov rax, rbx                     ; mov the result into rax
    cmp rax, 0
    jge callit
    
    mov rbx, -1
    mul rbx

    callit:
    call is_prime                    ; call if it is prime
    cmp rax, 1                       ; if it is, then
    je new_loop                      ; make another loop

    cmp r13, r11                     ; if n > max_prime
    jg update_values                 ; then update
    jmp continue                     ; else continue

    update_values:
    mov r11, r13                     ; mov n into max_prime
    mov rax, r15                     ; mov a into rax

    cmp rax, 0
    jl mult_less1
    jmp continue1

    mult_less1:
    mov rbx, -1
    mul rbx

    continue1:
    mov r10, rax

    cmp r14, 0
    jl mult_less2
    jmp multi

    mult_less2:
    mov rax, r14
    mov rbx, -1
    mul rbx

    multi:

    mov rax, r10
    mul r14                          ; a * b
    mov r12, rax                     ; mov the result into r12

    continue:
    inc r15                          ; rbx++
    cmp r15, 1000                    ; if is not 1000
    jne set_n                        ; continue

    inc r14                          ; else, rax++
    cmp r14, 999                     ; if rax == 999
    je break                         ; break the loop

    jmp set_b                        ; else, make another loop
    break:
    ret


; is_prime(rax: number) -> bool(rax)
is_prime:

    cmp rax, 1                       ; if is less or equal than
    jle end                          ; 1, then jump
    cmp rax, 2                       ; if is 2
    je prime                         ; then is prime
    cmp rax, 3                       ; if is 3
    je prime                         ; then is prime
    cmp rax, 5                       ; if is 5
    je prime                         ; then is prime

    mov rbx, rax                     ; mov the number into rbx
    mov rcx, 2                       ; mov th into rcx
    div rcx                          ; split the number

    prime_loop:
    mov rax, rbx                     ; mov the number into rax
    xor rdx, rdx                     ; clear the remander
    div rcx                          ; rax / rcx
    cmp rdx, 0                       ; if the remainder is 0
    je end                           ; the end the process
    inc rcx                          ; else inc rcx
    cmp rcx, rbx                     ; if im not in the last iter
    jne prime_loop                   ; continue
    jmp prime                        ; is prime!

    end: 
    mov rax, 0                       ; mov false into rax
    jmp skip                         ; skip prime

    prime:
    mov rax, 1                       ; mov true into rax

    skip:
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
