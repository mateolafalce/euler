section .data
    output: dq 0

section .text

global _start


_start: 

    call get_digits
    call set_variables
    call print_number
    call new_line
    call exit


get_digits:
    
    xor r12, r12
    mov r15, 2
    iter:
    call get_power
    cmp rax, r15
    je add_it
    jmp continue
    add_it:
    add r12, r15
    continue:
    inc r15
    cmp r15, 1000000
    jne iter

    ret


; get_power(r15) -> rax
get_power:

    xor r13, r13
    mov r14, r15

    mov rax, r14
    mov rbx, 100000
    xor rdx, rdx
    div rbx
    mov r11, rax
    call power
    add r13, rax

    mov rax, r11
    mov rbx, 100000
    mul rbx
    sub r14, rax

    mov rax, r14
    mov rbx, 10000
    xor rdx, rdx
    div rbx
    mov r11, rax
    call power
    add r13, rax

    mov rax, r11
    mov rbx, 10000
    mul rbx
    sub r14, rax

    mov rax, r14
    mov rbx, 1000
    xor rdx, rdx
    div rbx
    mov r11, rax
    call power
    add r13, rax

    mov rax, r11
    mov rbx, 1000
    mul rbx
    sub r14, rax

    mov rax, r14
    mov rbx, 100
    xor rdx, rdx
    div rbx
    mov r11, rax
    call power
    add r13, rax

    mov rax, r11
    mov rbx, 100
    mul rbx
    sub r14, rax

    mov rax, r14
    mov rbx, 10
    xor rdx, rdx
    div rbx

    mov r10, rdx

    mov r11, rax
    call power
    add r13, rax

    mov rax, r11
    mov rbx, 10
    mul rbx
    sub r14, rax

    mov rax, r10
    call power
    add r13, rax

    mov rax, r13

    ret


; power(rax: number) -> rax
power:

    xor rcx, rcx
    mov rbx, rax
    power_loop:
    mul rbx
    inc rcx
    cmp rcx, 4
    jne power_loop

    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
