section .data
    output: dq 0
    array TIMES 64 dq 0


section .text

global _start


_start: 

    call get_array
    call get_distinct
    call set_variables
    call print_number
    call new_line
    call exit


get_distinct:

    xor r14, r14                     ; iter=0..63
    xor r12, r12                     ; output
    get_loop:
    mov rax, [array+8*r14]
    call is_unique
    cmp rax, 1
    jne continue

    inc r12                          ; unique++

    continue:
    inc r14
    cmp r14, 64
    jne get_loop
    ret


; is_unique(rax: number) -> rax(bool)
is_unique:

    mov rbx, rax
    xor rdx, rdx
    xor rax, rax
    loop_uni:
    cmp rbx, [array+8*rdx]
    je equal
    jmp next

    equal:
    inc rax

    next:
    inc rdx
    cmp rdx, 64
    jne loop_uni

    cmp rax, 1
    jne is_ok

    mov rax, 0
    jmp end_eq

    is_ok:
    mov rax, 1

    end_eq:
    ret


get_array:

    xor r14, r14                     ; index
    mov r13, 2                       ; a
    mov r12, 2                       ; b
    loop:
    mov rax, r13
    mov rbx, r12
    call power
    mov [array + r14*8], rax

    cmp r13, 10
    jne inc_a

    mov r13, 2
    inc r12
    jmp next_iter

    inc_a:
    inc r13

    next_iter:
    inc r14
    cmp r14, 64
    jne loop

    ret


; power(rax: number, rbx: power) -> rax
power:

    
    cmp rbx, 0
    jle end

    xor rcx, rcx
    mov r15, rbx
    dec r15
    mov rbx, rax

    power_loop:
    mul rbx
    inc rcx
    cmp rcx, r15
    jne power_loop
    end:
    ret


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
