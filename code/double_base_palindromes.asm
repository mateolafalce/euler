section .data
    output: dq 0

section .text

global _start


_start: 

    ;call db_palindromes
    mov rcx, 11
    call is_palindromic
    mov r12, rax
    call set_variables
    call print_number
    call new_line
    call exit


db_palindromes:
    
    ret


; is_palindromic(rcx) -> rax(bool)
is_palindromic:

                                     ;[x| | | | | ]
    mov rax, rcx                     ; mov the numebr into rax
    mov rbx, 100000                  ; mov 100.000 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx
    mov r13, rax                     ; mov the result of division into r13
    
                                     ;[ | | | | |x]
    mov rax, rcx                     ; mov the number into rax
    mov rbx, 10                      ; mov 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx
    mov r12, rdx                     ; mov the remainder of division into r12

    cmp r12, r13                     ; if arent equal
    jne done                         ; its done

                                     ;[ |x| | | | ]
    mov rax, r13                     ; mov the result of division into rax 
    mov rbx, 100000                  ; mov rbx, 100.000
    mul rbx                          ; product = number * 100.000 
    mov r13, rax                     ; mov the product into r13 

    mov rax, rcx                     ; mov the number into rax
    sub rax, r13                     ; the number - greater
    mov r10, rax                     ; this will be used later

    mov rbx, 10000                   ; mov 10.000 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx
    mov r13, rax                     ; mov the result of division into r13

                                     ;[ | | | |x| ]
    mov rax, rcx                     ; mov the number into rax
    mov rbx, 100                     ; mov 100 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx
    mov r12, rdx                     ; mov the remainder of division into r12

    mov rax, r12                     ; mov the remainder into rax
    mov rbx, 10                      ; mov 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; result = rax / rbx
    mov r12, rax                     ; mov the result into r12

    cmp r12, r13                     ; if arent equal
    jne done                         ; its done

                                     ; [ | |x| | | ]
    mov rax, r13                     ; mov the result of division into rax 
    mov rbx, 10000                   ; mov rbx, 10.000
    mul rbx                          ; product = number * 10.000 
    sub r10, rax                     ; result = modify number - 2nd greater number

    mov rax, r10                     ; mod the result into rax
    mov rbx, 1000                    ; mov rbx, 1.000
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx
    mov r13, rax                     ; mov the result of division into r13

                                     ;[ | | |x| | ]
    mov rax, r13                     ; mov the number into rax
    mov rbx, 1000                    ; mov 1.000 into rbx
    mul rbx                          ; result = 3rd great number * 1.000
    sub r10, rax                     ; last great number - result

    mov rax, r10                     ; mov the result of sub into rax
    mov rbx, 100                     ; mov 100 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; rax / rbx
    mov r12, rax                     ; mov the result of division into r13

    cmp r12, r13                     ; if arent equal
    jne done                         ; its done

    mov rax, 1                       ; return 1, sucess
    jmp end                          ; end process

    done:
    xor rax, rax                     ; return 0
    end:
    ret  


print_number:

    call division

    cmp rax, 0                       ; if rax(result of div) == 0
    je check_if_is_need              ; jump to check_if_is_need

    cmp rax, 0                       ; if rax(result of div) != 0
    jne modify_flag                  ; jump to modify_flag

    check_if_is_need:
    cmp r15, 1                       ; if im in the last iter
    je modify_flag                   ; modify_flag
    cmp r11, 0                       ; if the flag is false,
    je jump_the_write                ; jump the write

    modify_flag:
    mov r11, 1                       ; change the state

    call write_number
    jump_the_write:
    call modify_number
    call modify_divisor

    dec r15                          ; decrement counter
    jnz print_number                 ; if counter not 0, then loop again
    ret


; set_variables_for_convert(r12:u64 -> number to print)
set_variables:

    mov r15, 20                      ; n iter
    mov r13, 10000000000000000000    ; max divisor, u64::MAX = 18446744073709551615
    mov r11, 0                       ; it will work as a flag for unused zeros
    ret


division:

    mov rax, r12                     ; store number content into rax
    mov rbx, r13                     ; store the max divisor into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; make the division
    mov r14, rax                     ; store the result of division into r14
    ret


modify_number:

    mov rax, r14                     ; set the result of divison into rax
    mov rbx, r13                     ; set the current max divisor into rbx
    mul rbx                          ; great_number = result of division * current divisor
    sub r12, rax                     ; number - great_number 
    ret


modify_divisor:

    mov rax, r13                     ; store divisor content into rax
    mov rbx, 10                      ; set 10 into rbx
    xor rdx, rdx                     ; clear remainder
    div rbx                          ; div divisor/10
    mov r13, rax                     ; mov result of division into divisor
    ret


write_number:

    add rax, '0'                     ; add +48 to get numbers in ASCII
    mov [output], rax                ; done, store result in "sum"
    mov rcx, 1                       ; len of a ascci char
    call write                       ; call write
    ret


new_line:

    mov rcx, 10                      ; mov \n to rcx
    mov [output], rcx                ; mov \n to [output]
    mov rcx, 1                       ; \n len  
    call write                       ; call write
    ret


; write([output] -> text, rcx -> text len)
write:

    mov rax, 1                       ; write(
    mov rdi, 1                       ;   STDOUT_FILENO,
    mov rsi, output                  ;   text,
    mov rdx, rcx                     ;   sizeof(text)
    syscall                          ; );
    ret


exit:

    mov rax, 60                      ; exit(
    mov rdi, 0                       ;   EXIT_SUCCESS
    syscall                          ; );
